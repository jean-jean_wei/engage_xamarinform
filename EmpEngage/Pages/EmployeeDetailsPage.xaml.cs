﻿using System;
using System.Collections.Generic;
using EmpEngage.Models;
using Xamarin.Forms;

namespace EmpEngage
{
	public partial class EmployeeDetailsPage : ContentPage
	{
		EmployeeDetails _details;

		public EmployeeDetailsPage(EmployeeDetails details)
		{
			_details = details;
			InitializeComponent();
			Title = "Info";
			// assign employee details  to XAML
			firstLastName.Text = _details.FirstLastName;
			jobTitle.Text = _details.JobTitle;
			phone.Text = _details.Phone;
			email.Text = _details.EmailAddress;
			branch.Text = _details.Branch;

		}


		void OnGetManager(object sender, EventArgs e)
		{
		}

		void OnGetTeam(object sender, EventArgs e)
		{
		}
	}
}
