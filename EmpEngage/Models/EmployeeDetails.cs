﻿using System;

namespace EmpEngage.Models
{
	public class EmployeeDetails
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Name { get; set; }
		public string Phone { get; set; }
		public string CellPhone { get; set; }
		public string JobTitle { get; set; }
		public string Branch { get; set; }
		public string MailingAddress { get; set; }
		public string City { get; set; }
		public string PostalCode { get; set; }
		public string EmailAddress { get; set; }
		public string FirstLastName { get { return FirstName + " " + LastName; } }

		public EmployeeDetails Clone()
		{
			var cloneToReturn = new EmployeeDetails();
			cloneToReturn.FirstName = FirstName;
			cloneToReturn.LastName = LastName;
			cloneToReturn.Name = Name;
			cloneToReturn.Phone = Phone;
			cloneToReturn.CellPhone = CellPhone;
			cloneToReturn.JobTitle = JobTitle;
			cloneToReturn.Branch = Branch;
			cloneToReturn.MailingAddress = MailingAddress;
			cloneToReturn.City = City;
			cloneToReturn.PostalCode = PostalCode;
			cloneToReturn.EmailAddress = EmailAddress;

			return cloneToReturn;
		}
	}
}
