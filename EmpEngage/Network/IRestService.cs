﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EmpEngage.Models;

namespace EmpEngage.Network
{
	public interface IRestService
	{
		Task<List<EmployeeDetails>> RefreshDataAsync (string url);
	}
}
