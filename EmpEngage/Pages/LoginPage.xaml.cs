﻿using System;
using System.Collections.Generic;
using EmpEngage.Network;

using Xamarin.Forms;

namespace EmpEngage
{
	public partial class LoginPage : ContentPage
	{
		public LoginPage()
		{
			InitializeComponent();
		}

		void OnLogin(object sender, EventArgs e)
		{
			//TODO: implement conditions related to login button to prevent null or invalid user inputs
			if (string.IsNullOrEmpty(this.emailAddress.Text) || string.IsNullOrEmpty(this.password.Text))
			{
				return;
			}


			HttpClientManger.Instance.LoginEmail = this.emailAddress.Text;
			HttpClientManger.Instance.Password = this.password.Text;

			bool result = LoginControl.Login();
			if (result)
			{
				Navigation.PushAsync(new EmployeeListPage());
			}
		}
	}
}
