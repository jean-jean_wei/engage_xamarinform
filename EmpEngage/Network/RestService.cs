﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using EmpEngage.Models;
using Newtonsoft.Json;

namespace EmpEngage.Network
{
	public class RestService
	{
		HttpClient client;

		public RestService()
		{
			var authData = string.Format("{0}:{1}", HttpClientManger.Instance.LoginEmail, HttpClientManger.Instance.Password);
			var authHeaderValue = Convert.ToBase64String(Encoding.UTF8.GetBytes(authData));

			client = new HttpClient();
			client.MaxResponseContentBufferSize = 256000;
			client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);
		}



		public async Task<List<EmployeeDetails>> RefreshDataAsync(string url)
		{
			var items = new List<EmployeeDetails>();

			var uri = new Uri(string.Format(url, string.Empty));

			try
			{
				var response = await client.GetAsync(uri);
				if (response.IsSuccessStatusCode)
				{
					var content = await response.Content.ReadAsStringAsync();
					items = JsonConvert.DeserializeObject<List<EmployeeDetails>>(content);
				}
			}
			catch (Exception ex)
			{
				Debug.WriteLine(@"ERROR {0}", ex.Message);
			}

			return items;
		}
	}
}
