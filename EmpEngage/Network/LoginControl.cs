﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using Newtonsoft.Json.Linq;

namespace EmpEngage.Network
{
	public static class LoginControl
	{
		private static ManualResetEvent allDone = new ManualResetEvent(false);

		public static bool Login()
		{
			var success = true;

			try
			{

				var url = "https://login.windows.net/aircanada.ca/oauth2/token";//soap.ServerUrl+"/services/oauth2/token";
				HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

				//Set the content type and method
				request.ContentType = "application/x-www-form-urlencoded";
				request.Method = "POST";
				// start the asynchronous operation
				request.BeginGetRequestStream(new AsyncCallback(GetRequestStreamCallback), request);
				// Keep the main thread from continuing while the asynchronous
				// operation completes. A real world application
				// could do something useful such as updating its user interface. 
				allDone.WaitOne();

			}
			catch
			{
				success = false;
			}
			return success;
		}

		private static void GetRequestStreamCallback(IAsyncResult asynchronousResult)
		{
			HttpWebRequest request = (HttpWebRequest)asynchronousResult.AsyncState;

			// End the operation
			Stream postStream = request.EndGetRequestStream(asynchronousResult);
			string postData = "grant_type=password&client_id=ff866eeb-b331-45db-9e3d-ed010fb5ed47&client_secret=mGyhjFUoVmWJHjg5mPhgQ0e8/hVmaclVuOgMnS3ek6w=&resource=https://graph.windows.net&username=" + HttpClientManger.Instance.LoginEmail + "&password=" + HttpClientManger.Instance.Password;

			//Debug.WriteLine("Please enter the input data to be posted:");
			//string postData = Debug.ReadLine();

			// Convert the string into a byte array.
			byte[] byteArray = Encoding.UTF8.GetBytes(postData);

			// Write to the request stream.
			postStream.Write(byteArray, 0, postData.Length);
			postStream.Dispose();

			// Start the asynchronous operation to get the response
			request.BeginGetResponse(new AsyncCallback(GetResponseCallback), request);
		}

		private static void GetResponseCallback(IAsyncResult asynchronousResult)
		{
			HttpWebRequest request = (HttpWebRequest)asynchronousResult.AsyncState;

			// End the operation
			HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(asynchronousResult);
			Stream streamResponse = response.GetResponseStream();
			StreamReader streamRead = new StreamReader(streamResponse);
			string responseString = streamRead.ReadToEnd();
			var result = JObject.Parse(responseString);
			HttpClientManger.Instance.AccessToken = (string)result["access_token"];
			Debug.WriteLine(responseString);
			// Close the stream object
			streamResponse.Dispose();
			streamRead.Dispose();

			// Release the HttpWebResponse
			response.Dispose();
			allDone.Set();
		}
	}
}
