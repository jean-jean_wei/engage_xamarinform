﻿using System;
using System.Collections.Generic;
using EmpEngage.Models;
using System.Threading.Tasks;

namespace EmpEngage.Network
{
	public class HttpClientManger
	{
		#region thread-safe singleton implementation, source: http://msdn.microsoft.com/en-us/library/ff650316.aspx
		private static volatile HttpClientManger _instance;
		private static readonly object SyncRoot = new Object();


		/// <summary>
		/// Initializes a new instance of the <see cref="Buzz.Retail.iOS.NavigationManager"/> class.
		/// </summary>
		private HttpClientManger() { }

		/// <summary>
		/// Gets the instance.
		/// </summary>
		/// <value>The instance.</value>
		public static HttpClientManger Instance
		{
			get
			{
				if (_instance == null)
				{
					lock (SyncRoot)
					{
						if (_instance == null)
						{
							_instance = new HttpClientManger();
							_instance.restService = new RestService();
						}
					}
				}
				return _instance;
			}
		}
		#endregion

		#region Properties
		public string LoginEmail { get; set; }
		public string Password { get; set; }
		public string AccessToken { get; set; }

		RestService restService;
		#endregion

		public Task<List<EmployeeDetails>> GetEmployDetailsList()
		{
			var url = "https://microsoft-apiappf5431d03d33647e6a0a77444b2d19d96.azurewebsites.net:443/api/managerSearch?Query=" + LoginEmail + "&Token=" + AccessToken;

			return restService.RefreshDataAsync(url); 
		}

	}
}
