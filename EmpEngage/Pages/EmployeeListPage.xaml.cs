﻿using System;
using System.Collections.Generic;
using EmpEngage.Network;
using Xamarin.Forms;
using EmpEngage.Models;
using System.Diagnostics;

namespace EmpEngage
{
	public partial class EmployeeListPage : ContentPage
	{
		List<EmployeeDetails> records;

		public EmployeeListPage()
		{
			InitializeComponent();
			this.EmployeeListView.ItemTemplate = new DataTemplate(typeof(CustomImageCell));
			Title = "Contact";
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			RefreshDataAsync();
		}

		public async void RefreshDataAsync()
		{

			try
			{
					EmployeeListView.BeginRefresh();
					EmployeeListView.ItemsSource = await HttpClientManger.Instance.GetEmployDetailsList();;
					EmployeeListView.EndRefresh();
			}
			catch (Exception ex)
			{
				Debug.WriteLine(@"				ERROR {0}", ex.Message);
			}

		}
			void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
		{
			var details = e.SelectedItem as EmployeeDetails;
			Navigation.PushAsync(new EmployeeDetailsPage(details.Clone()));

		}
	}
}
